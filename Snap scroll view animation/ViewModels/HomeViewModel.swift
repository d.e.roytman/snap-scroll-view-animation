//
//  HomeViewModel.swift
//  Snap scroll view animation
//
//  Created by Dmitry Roytman on 23.02.2022.
//

import SwiftUI

final class HomeViewModel: ObservableObject {
  
  // MARK: - Internal properties
  
  @Published var lineItems: [LineItem]
  @Published var offset: CGFloat
  
  var topPaddingOffset: CGFloat { .offset * offsetMultiplier }
  
  // MARK: - Private properties
  
  private var offsetMultiplier: CGFloat { .init(lineItems.count - 1) }
  private let delegate: ScrollViewDelegate
  
  // MARK: - init
  
  init(
    lineItems: [LineItem] = .lineItems,
    offset: CGFloat = .zero,
    delegate: ScrollViewDelegate = .init()
  ) {
    self.lineItems = lineItems
    self.offset = offset
    self.delegate = delegate
  }
  
  // MARK: - Internal methods
   
  func onAppear() {
    UIScrollView.appearance().delegate = delegate
    UIScrollView.appearance().bounces = false
  }
  func onDisappear() {
    UIScrollView.appearance().delegate = nil
    UIScrollView.appearance().bounces = true
  }
}
