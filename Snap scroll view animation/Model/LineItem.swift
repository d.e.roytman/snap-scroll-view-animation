//
//  LineItem.swift
//  Snap scroll view animation
//
//  Created by Dmitry Roytman on 23.02.2022.
//

import Foundation

struct LineItem: Identifiable {
  let id = UUID()
  let title: String
  let logo: String
}

extension Array where Element == LineItem {
  static var lineItems: [Element] {
    return [
      LineItem(title: "paperplane", logo: "paperplane"),
      LineItem(title: "folder", logo: "folder"),
      LineItem(title: "tray", logo: "tray"),
      LineItem(title: "trash", logo: "trash"),
      LineItem(title: "doc", logo: "doc"),
      LineItem(title: "terminal", logo: "terminal"),
    ]
  }
}
