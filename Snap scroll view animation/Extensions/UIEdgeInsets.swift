//
//  UIEdgeInsets.swift
//  Snap scroll view animation
//
//  Created by Dmitry Roytman on 23.02.2022.
//

import UIKit

extension UIEdgeInsets {
  static var safeAreaInsets: UIEdgeInsets {
    guard let scene = UIApplication.shared.connectedScenes.first as? UIWindowScene else { return .zero }
    guard let window = scene.windows.first else { return .zero }
    return window.safeAreaInsets
  }
}
