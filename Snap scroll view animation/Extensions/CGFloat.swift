//
//  CGFloat.swift
//  Snap scroll view animation
//
//  Created by Dmitry Roytman on 23.02.2022.
//

import UIKit

extension CGFloat {
  static var rowHeigth: CGFloat { 80 }
  static var offset: CGFloat { .rowHeigth }
  static var snapInterval: CGFloat { .rowHeigth }
}

