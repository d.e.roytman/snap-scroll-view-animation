//
//  OffsetPreferenceKey.swift
//  Snap scroll view animation
//
//  Created by Dmitry Roytman on 23.02.2022.
//

import SwiftUI

// MARK: - Offset preference key

struct OffsetPreferenceKey: PreferenceKey {
  static var defaultValue: CGFloat = .zero
  static func reduce(value: inout CGFloat, nextValue: () -> CGFloat) {
    value = nextValue()
  }
}
