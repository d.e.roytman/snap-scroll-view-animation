//
//  OffsetViewModifier.swift
//  Snap scroll view animation
//
//  Created by Dmitry Roytman on 23.02.2022.
//

import SwiftUI
// MARK: - Offset view modifier

struct OffsetViewModifier: ViewModifier {
  @Binding var offset: CGFloat
  
  func body(content: Content) -> some View {
    content
      .overlay {
        GeometryReader { proxy in
          Color.clear
            .preference(
              key: OffsetPreferenceKey.self,
              value:
                proxy.frame(
                  in: .named(CoordinateSpaceName.scrollSpace)
                )
                .minY
            )
        }
      }
      .onPreferenceChange(OffsetPreferenceKey.self) { offset = $0 }
  }
  
}
