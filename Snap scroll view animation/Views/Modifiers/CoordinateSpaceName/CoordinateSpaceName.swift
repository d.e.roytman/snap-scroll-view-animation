//
//  CoordinateSpaceName.swift
//  Snap scroll view animation
//
//  Created by Dmitry Roytman on 23.02.2022.
//

import Foundation

// MARK: - Coordinate space name

struct CoordinateSpaceName: Hashable {
  let spaceName: String
}

extension CoordinateSpaceName {
  static var scrollSpace: CoordinateSpaceName { CoordinateSpaceName(spaceName: .coordinateSpaceName) }
}

extension String {
  static var coordinateSpaceName: String { "SCROLL_NAME_SPACE" }
}
