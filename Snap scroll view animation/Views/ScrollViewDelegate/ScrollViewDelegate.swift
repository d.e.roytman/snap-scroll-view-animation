//
//  ScrollViewDelegate.swift
//  Snap scroll view animation
//
//  Created by Dmitry Roytman on 23.02.2022.
//

import UIKit

// MARK: - Scroll view delegate

final class ScrollViewDelegate: NSObject, UIScrollViewDelegate, ObservableObject {
  func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
    let target = scrollView.contentOffset.y
    let condition = (target / .snapInterval).rounded(.toNearestOrAwayFromZero)
    scrollView.setContentOffset(.init(x: .zero, y: condition * .snapInterval), animated: true)
  }
  
  func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
    let target = targetContentOffset.pointee.y
    let condition = (target / .snapInterval).rounded(.toNearestOrAwayFromZero)
    scrollView.setContentOffset(.init(x: .zero, y: condition * .snapInterval), animated: true)
  }
}
