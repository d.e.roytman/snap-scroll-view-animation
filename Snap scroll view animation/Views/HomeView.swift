//
//  HomeView.swift
//  Snap scroll view animation
//
//  Created by Dmitry Roytman on 23.02.2022.
//

import SwiftUI

struct HomeView: View {
  @StateObject var viewModel: HomeViewModel = .init()
  
  var body: some View {
    ScrollView(showsIndicators: false) {
      ScrollingContentView(isRowHighlighted: false)
        .environmentObject(viewModel)
        .padding(.top, .padding)
        .overlay(
          GeometryReader { proxy in
            ScrollingContentView(isRowHighlighted: true)
              .environmentObject(viewModel)
          }
            .frame(height: .rowHeigth)
            .offset(y: viewModel.offset)
            .clipped()
            .background(.yellow)
            .padding(.top, .padding)
            .offset(y: -viewModel.offset)
          , alignment: .top
        )
        .modifier(OffsetViewModifier(offset: $viewModel.offset))
        .padding(.bottom, viewModel.topPaddingOffset)
        .onAppear {
          viewModel.onAppear()
        }
        .onDisappear {
          viewModel.onDisappear()
        }
    }
    .frame(maxWidth: .infinity, maxHeight: .infinity)
    .ignoresSafeArea()
    .background(.brown)
    .coordinateSpace(name: CoordinateSpaceName.scrollSpace)
  }
}

// MARK: - Preview

struct HomeView_Previews: PreviewProvider {
  static var previews: some View {
    HomeView()
  }
}

// MARK: - Helper

extension CGFloat {
  fileprivate static var padding: CGFloat {
    UIScreen.main.bounds.height / 3
  }
}
