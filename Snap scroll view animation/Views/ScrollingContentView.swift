//
//  ScrollingContentView.swift
//  Snap scroll view animation
//
//  Created by Dmitry Roytman on 23.02.2022.
//

import SwiftUI

struct ScrollingContentView: View {
  @EnvironmentObject var viewModel: HomeViewModel
  let isRowHighlighted: Bool
  
  var body: some View {
    VStack(spacing: .zero) {
      ForEach(viewModel.lineItems) { lineItem in
        HStack {
          Text(lineItem.title.capitalized)
            .font(.title)
            .foregroundColor(isRowHighlighted ? .brown : .yellow)
          
          Spacer()
          Image(systemName: lineItem.logo)
            .resizable()
            .renderingMode(.template)
            .aspectRatio(contentMode: .fit)
            .frame(width: .frameWidth, height: .frameHeight)
            .foregroundColor(isRowHighlighted ? .brown : .yellow)
        }
        .padding(.horizontal, .horizontalPadding)
        .frame(height: .rowHeigth)
      }
    }
    .padding(.top, .horizontalTop)
    .padding(.bottom, .horizontalBottom)
  }
}

// MARK: - Preview

struct ScrollingContentView_Previews: PreviewProvider {
    static var previews: some View {
      ScrollingContentView(isRowHighlighted: true)
        .environmentObject(HomeViewModel())
    }
}

// MARK: - Helper

extension CGFloat {
  fileprivate static var horizontalPadding: CGFloat { 15 }
  fileprivate static var horizontalTop: CGFloat { UIEdgeInsets.safeAreaInsets.top }
  fileprivate static var horizontalBottom: CGFloat { UIEdgeInsets.safeAreaInsets.bottom }
  fileprivate static var frameWidth: CGFloat { 100 }
  fileprivate static var frameHeight: CGFloat { 40 }
}
