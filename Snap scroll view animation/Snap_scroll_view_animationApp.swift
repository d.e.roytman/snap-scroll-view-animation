//
//  Snap_scroll_view_animationApp.swift
//  Snap scroll view animation
//
//  Created by Dmitry Roytman on 23.02.2022.
//

import SwiftUI

@main
struct Snap_scroll_view_animationApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
